const puppeteer = require('puppeteer');
const prompt = require('prompt');
const path = require('path');
const nodemailer = require("nodemailer");

function handleError(error) {
  console.log(error);
  return 1;
}


async function sendEmail(content, from, to) {
  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
    sendmail: true,
    newline: "unix",
    path: "/usr/sbin/sendmail"
  });

  try {
    // send mail with defined transport object
    let info = await transporter.sendMail({
      from: from,
      to: to,
      subject: "appointment available!",
      text: content
    });
    return info;
  } catch(e) {
    return e;
  }
}

async function run(location, appointmentBefore, child, emailTo, emailFrom, screenshotsDir) {
  try {
    const browser = await puppeteer.launch({
      ignoreHTTPSErrors: true,
      args: [
        '--no-sandbox',
        '--disable-setuid-sandbox',
        // This will write shared memory files into /tmp instead of /dev/shm,
        // because Docker’s default for /dev/shm is 64MB
        '--disable-dev-shm-usage'
      ],
    });
    const page = await browser.newPage();
    const startURL = "https://www.politiet.no/tjenester/pass-og-id-kort"+
      "/bestille-endre-eller-avbestille-time-til-pass-og-id-kort/";
    const defaultTimeoutMilliSeconds = 3000;
    const longTimeoutMilliSeconds = 30000;
    const animationTimeoutMilliSeconds = 200;

    const wantAppBefore = new Date(appointmentBefore);
    console.log(`want appointment before: ${wantAppBefore.toDateString()}`);

    try {
      // navigate and wait
      await page.goto(startURL)
      console.log("we're on the start page!")
      const getOfficesButtonSelector = 'button.autocomplete__search-btn'
      const getOfficesButton = await page.waitForSelector(getOfficesButtonSelector, {
        timeout: defaultTimeoutMilliSeconds
      })

      // type in our address
      await page.type("input#autocomplete__input", location, {delay: 40})
      console.log("we have typed in the location!")
      try {
        const officeSelector = "div.find-org-service__result"
        await Promise.all([
          getOfficesButton.click(),
          page.waitForSelector(officeSelector, { timeout: defaultTimeoutMilliSeconds } ),
        ])
        console.log("we have pushed the button!")
      } catch(e) {
        console.log(e)
      }
      await page.screenshot({ path: path.join(screenshotsDir,
        '/searched-for-offices.png') })

      // choose the first suggested office, which is the nearest
      const resultButtonSelector = ".find-org-service__accordion "+
        "button.accordion__button"
      const nearestOfficeButton = await page.waitForSelector(resultButtonSelector, {
        timeout: defaultTimeoutMilliSeconds
      })
      console.log("selected nearest office!")
      await nearestOfficeButton.click()
      const startBookingAppointmentSelector = ".accordion__region--expanded "+
        "a.call-to-action--button"
      const startBookingAppointmentLink = await page.waitForSelector(
        startBookingAppointmentSelector, { timeout: defaultTimeoutMilliSeconds })
      console.log("found booking button!")
      await Promise.all([
        startBookingAppointmentLink.click(),
        page.waitForNavigation({
          timeout: longTimeoutMilliSeconds,
          waitUntil: ["load", "networkidle0"]
        })
      ])
      console.log("we're in the booking process!")
      await page.screenshot({ path: path.join(screenshotsDir,
        '/started-booking.png') })

      const activeOptionsQuery = ".v-expansion-panel__container--active "+
        "li.v-expansion-panel__container";
      const activeOptions = await page.$$(activeOptionsQuery);
      const isPassOption = await Promise.all(activeOptions.map(async opt =>
        await opt.$eval("label", label => {
          return label.innerHTML === "Kun pass";
        })
      ));
      const passOption = activeOptions.filter((_, index) => {
        return isPassOption[index]
      })[0];
      console.log("found only pass option ...");
      await passOption.$eval("input", async input => await input.click());
      await page.screenshot({ path: path.join(screenshotsDir,
        '/chose-pass-only.png') });
      console.log("... and chose it!");
      const personPartialClass = (child ? "child" : "adult")
      const personClass = ".choose-wrapper." + personPartialClass;
      const personOptionButtonsSelector =
        `.v-expansion-panel__body ${personClass} button`;
      const personButtons = await passOption.$$(personOptionButtonsSelector);
      const isPlusButton = await Promise.all(personButtons.map(async but =>
        await but.$eval("i", icon => {
          return icon.innerHTML === "add";
        })
      ));
      const personPlusButton = personButtons.filter((_, index) => {
        return isPlusButton[index];
      })[0];
      console.log(`found plus button for ${personPartialClass}...`);
      await personPlusButton.click();
      console.log("... and pressed it!");
      await page.waitForTimeout(100);
      await page.screenshot({ path: path.join(screenshotsDir,
        '/add-person.png') });
      const stepHeaders = await page.$$("li.v-expansion-panel__container.process-step-header");
      const isStep3 = await Promise.all(stepHeaders.map(async header =>
        await header.$eval("div.process-number", div => div.innerHTML == "3")
      ));
      const thirdStepHeader = stepHeaders.filter((_, index) => isStep3[index])[0];
      const thirdStepExpanderClass = ".v-expansion-panel__header__icon";
      const thirdStepExpander = await thirdStepHeader.$(thirdStepExpanderClass);
      await Promise.all([
        thirdStepExpander.click(),
        page.waitForTimeout(animationTimeoutMilliSeconds)
      ])
      console.log("moving on to date selection")
      await page.screenshot({ path: path.join(screenshotsDir,
        '/dates.png') });
      while (true) {
        const tableHeader = await page.$(".v-date-picker-header")
        const monthButton = await tableHeader.$(".v-date-picker-header__value button")
        const currentMonthText = await monthButton.evaluate(node => node.innerHTML)
        console.log(`looking at ${currentMonthText}`)
        const currentMonth = new Date(currentMonthText)
        const availableDatesSelector = ".v-date-picker-table button:not(.v-btn--disabled)"
        const availableDates = await page.$$(availableDatesSelector)
        if (availableDates.length == 0) {
          console.log("no appointments available this month...");
          if (currentMonth.getFullYear() > wantAppBefore.getFullYear() ||
              currentMonth.getMonth() >= wantAppBefore.getMonth() ) {
            console.log(`No appointment available before `+
              `${wantAppBefore.toDateString()}, exiting!`);
            return 1;
          }
          console.log("moving on to next month ...");
          const changeMonthButtons = await tableHeader.$$("button.v-btn--icon");
          const isNextButton = await Promise.all(changeMonthButtons.map(async but =>
            await but.$eval("i", icon => {
              return icon.innerHTML === "chevron_right";
            })
          ));
          const nextMonthButton = changeMonthButtons.filter((_, index) => {
            return isNextButton[index];
          })[0];
          await Promise.all([
            nextMonthButton.click(),
            page.waitForTimeout(5*defaultTimeoutMilliSeconds)
          ]);
          continue;
        }
        await page.screenshot({ path: path.join(screenshotsDir,
          '/available-dates.png') });
        const availableDaysStrings = await Promise.all(availableDates.map(async date =>
          await date.$eval(".v-btn__content", but => but.innerHTML)
        ));
        console.log("available dates:");
        console.log(availableDaysStrings);
        const availableDays = availableDaysStrings.map(Number)
        const earliestAvailableDay = Math.min(...availableDays)
        console.log(`earlierst available day: ${earliestAvailableDay}`);
        if (currentMonth.getMonth() == wantAppBefore.getMonth() &&
          earliestAvailableDay >= wantAppBefore.getDate()) {
            console.log(`Earliest appointment is ${earliestAvailableDay}. `+
              `${currentMonth}, which is too late for us. exiting!`);
            return 1;
        }
        const availableAppointment = earliestAvailableDay + " " + currentMonthText
        const emailText = `There is an appointment available before ${appointmentBefore}\n` +
          `on ${availableAppointment} - go book it now!`
        console.log(emailText);
        const emailSent = await sendEmail(emailText, emailFrom, emailTo);
        console.log(`attempt to send email with above text to ${emailTo} returned:`);
        console.log(emailSent);
        break;
      }
    } catch(e) {
      await page.screenshot({ path: path.join(screenshotsDir, '/failed.png') })
        .catch(e => console.log("couldn't take screenshot - " + e))
      throw e
    } finally {
      browser.close();
    }
  } catch(e) {
    return handleError(e)
  }
  return 0
}

async function getInput(varName) {
  let input = process.env[varName]
  if (input == undefined) {
    input = await prompt.get(varName)
  }
  return input
}

async function main() {

  prompt.start()
  const location = process.env["WANT_APPOINTMENT_NEAR"] || "Bergen, Vestland"
  const appointmentBefore = await getInput("WANT_APPOINTMENT_BEFORE_DATE")
  const child = process.env["WANT_APPOINTMENT_FOR_CHILD"] || true
  const screenshotsDir = process.env["SCREENSHOTS_DIR"] || "."
  const emailTo = await getInput("SEND_ALERT_TO")
  const emailFrom = emailTo

  const returncode = await run(location, appointmentBefore, child, emailTo,
    emailFrom, screenshotsDir)
  process.exit(returncode);
}

main()
