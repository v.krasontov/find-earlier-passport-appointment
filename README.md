# police-checker

Service that checks whether there is a free appointment with the police for
getting your passport, and notifies you by mail if there is before a given
date.

## Features

Uses a headless browser to navigate the appointment booking system for passport
appointments on politi.no. Tries to find the earliest appointment avilable
before a given date, and alerts by email if such an appointment exists.
Obviously this is quite brittle.

## Prerequisites

This script sends an email, using `sendmail` - you should setup an `msmtp`
config (`~/.msmtprc`).

## Usage

```
npm i
node index.js
```

## Limitations

The docker image has not been updated to reflect the added feature of sending
email.

You can only find appointments for one adult or one child at a time.

## Debugging

The tool takes screenshots of the browser. These are stored in the directory
`/screenshots`. If you want to look at them, start the container without the
`--rm` flag, and copy the images out of that directory afterwards, or bind in a
local host directory into the `/screenshots` path.

When running locally, the screenshots will by default be saved in the current
working dir.

## Config

The following environment vars may be supplied to the script (default value in
brackets):

- `WANT_APPOINTMENT_NEAR` [`Bergen, Vestland`] - script will choose the closest
  station to the given location
- `WANT_APPOINTMENT_BEFORE_DATE` - we want an appointment earlier than this
  data. should be in format `YY-MM-DD`.
- `WANT_APPOINTMENT_FOR_CHILD` [`true`] - whether to look for an appointment
    for an adult or a child
- `SCREENSHOTS_DIR` [`.`] - location for storing screenshots
- `SEND_ALERT_TO` - email address to send notification to about available
    appointment
